(function () {

    angular
        .module("BookApp")
        .config(BookConfig);

    function BookConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("searchbooks", {
                url: "/searchbooks",
                templateUrl: "/views/search.html",
                controller: "SearchCtrl as ctrl"
            })
            .state("editbook", {
                url: "/editbook/:bookId",
                templateUrl: "/views/editbook.html",
                controller: "EditCtrl as ctrl"
            });

        $urlRouterProvider.otherwise("/searchbooks");
    }

    BookConfig.$inject = ["$stateProvider", "$urlRouterProvider"];


})();
