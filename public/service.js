(function () {
    angular.module("BookApp")
        .service("dbService", dbService);

    function dbService($http, $q) {
        var vm = this;

        vm.searchBooks = function (query) {
            var defer = $q.defer();
            
            console.log("Services - Search Book: query title= %s author= %s",query.title,query.author );
            
            $http.get("/api/books/" + query.title + "/" + query.author)        
            .then(function (result) {
                defer.resolve(result.data);
            }).catch(function (err) {
                defer.reject(err);
            });

            return defer.promise;
        };

        vm.editBook = function (bookId) {
            var defer = $q.defer();

            console.log("Service - Edit Book: %s", bookId);

            $http.post("/api/books/" + bookId)
                .then(function (book) {
                    console.log("Service Reply Result:",book.data);
                    defer.resolve(book.data);
                })
                .catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        };

        vm.saveBook = function (book) {
            var defer = $q.defer();

            console.log("Service - Save Book", book);

            $http.post("/api/save", {
                params: {
                    book: JSON.stringify(book),
                    headers: {'Content-type': 'application/json'}
                }}).then(function(result){
                defer.resolve(result);
                console.info("Service: update book success.");
            }).catch(function(err){
                defer.reject(err);
                console.info("Error in update book");
            });
            return defer.promise;

        };




    } //function dbService

    dbService.$inject = ["$http", "$q"];
})();

