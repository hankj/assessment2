(function () {
    angular.module("BookApp")
        .controller("SearchCtrl", SearchCtrl)
        .controller("EditCtrl", EditCtrl);

    SearchCtrl.$inject = ["dbService"];
    EditCtrl.$inject = ["$stateParams", "dbService"];

//Search Books controller
    function SearchCtrl(dbService) {
        console.log("Controller: SearchCtrl....")

        var vm = this;
        vm.query = {
            title: "",
            author: ""           
        }
        vm.books = [];
        vm.book = {};

        vm.searchBooks = function() {

            // dbService.searchBooks()
            dbService.searchBooks(vm.query)
                .then(function (books) {
                    vm.books = books;
                    console.log("Search Books result:", vm.books);
                })
                .catch(function (err) {
                    console.log("Some Error", err);
                });

        } //end vm.searchBook
    }

//Edit Book controller
    function EditCtrl($stateParams, dbService) {
        var vm = this;

        vm.saveBook = function() {

            console.log("Edit Controller - Saving Book", vm.book);
            dbService.saveBook(vm.book)
                .then(function (result) {
                    console.log("Service - Save book success");
                })
        };

        dbService.editBook($stateParams.bookId)
            .then(function (book) {
                vm.book = book;
            })

    }

}());